
from abc import ABC, abstractmethod
import pandas as pd
from src.eval_metrics import STEvaluator


class BaselineStratage(ABC):

    @abstractmethod
    def predict(self, window : pd.DataFrame, **kwargs) -> (int, object):
        '''
        :param window: DataFrame to predict next day's action
        :param kwargs:
        :return: (0, None) - buy,  (1, None) - sell
        '''
        pass


class BS_NaiveHold(BaselineStratage):

    def predict(self, window : pd.DataFrame, **kwargs):
        return (0, None)


class BS_AverageDayLine(BaselineStratage):

    def predict(self, window : pd.DataFrame, **kwargs):

        if window[-1] > sum(window) / len(window):
            return (0, None)
        else:
            return (1, None)

        return (0, None)


###################


if __name__ == "__main__":

    PRJ_PATH = "../../"
    DATA_PATH = PRJ_PATH + "stock-price-predictor/data/"
    PREDICT_PATH = PRJ_PATH + "stock-price-predictor/output/lstm_v1_a/"
    TARGET_STOCK = DATA_PATH + "NFLX.csv"

    split_date = "2017-12-30"

    # eval
    evaluator = STEvaluator(TARGET_STOCK, splite_date=split_date)
    trade_hist, asset_hist, metrics = evaluator.evaluate(BS_NaiveHold(), window_size=30)

    print(metrics)


























