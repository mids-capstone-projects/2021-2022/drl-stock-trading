from collections import defaultdict
from datetime import datetime

import src.config
import os

import pandas as pd
from src.stockenv import StockEnv
from src.config import DRL_MODELS, ST_REPORT
from src.stock_trading import StockTrader
from src.eval_metrics import STEvaluator
from src.baseline import BS_NaiveHold


def append_report_item(report, metrics, **kwargs):

    for k, v in kwargs.items():
        report[k].append(v)

    for k, v in metrics.items():
        report[k].append(v)

    return report


def find_stock_file(data_path, stock_name):
    for name in os.listdir(data_path):
        if name.startswith(stock_name + "_") and name.endswith(".csv"):
            return f"{data_path}/{name}"
    return ""


def run_config(model_config : dict):

    DEFAULT_KEY = "default"

    default = model_config[DEFAULT_KEY]

    cfg_name = default["name"]

    report = defaultdict(list)

    for name, cfg in model_config.items():

        print(f"Run =====> {name}")

        if name == DEFAULT_KEY:
            continue

        data = find_stock_file(default['DATA'], cfg['stock'])

        if name.startswith("BASELINE"):

            # Baseline
            evaluator = STEvaluator(data, splite_date=default["splite_date"])
            trade_hist, asset_hist, metrics = evaluator.evaluate(cfg['algo'](), window_size = cfg['state_len'])
            append_report_item(report, metrics, name=name, algo=cfg['algo'].__name__, type="", step="", stock = cfg['stock'], CP="")

        else:

            checkpoint = default['CP'] + name + ".zip"

            if "predict" in cfg.keys():
                predict = cfg["predict"]
            else:
                predict = False


            trader = StockTrader(data,
                                 cfg['algo'],
                                 name = name,
                                 stock_state_len = cfg['state_len'],
                                 start_data = default["start_date"],
                                 splite_date = default["splite_date"],
                                 stock_with_predict=predict)

            trader.load_or_train_and_save(path = checkpoint,
                                          policy_tpye = "MlpPolicy",
                                          total_timesteps = cfg['step'])

            # Evaluate
            evaluator = STEvaluator(data, splite_date=default["splite_date"])
            trade_hist, asset_hist, metrics = evaluator.evaluate(trader.policy,
                                                                 trader.used_columns(),
                                                                 with_prediction= predict,
                                                                 window_size=cfg['state_len'])

            append_report_item(report,
                               metrics,
                               name = name,
                               algo = cfg['algo'].__name__,
                               type = cfg['type'],
                               step = cfg['step'],
                               stock = cfg['stock'],
                               CP = checkpoint)

    #======================================================

    output_path = ST_REPORT \
                  + "st_report_" \
                  + cfg_name + "_" \
                  + datetime.now().strftime("%Y_%m_%d_%H_%M_%S") \
                  + ".csv"

    df_report = pd.DataFrame(data = report)
    df_report.to_csv(output_path, index=False)



if __name__ == "__main__":
    run_config(DRL_MODELS)






