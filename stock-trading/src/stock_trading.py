

import os

from src.stockenv import StockEnv
from src.eval_metrics import STEvaluator
from src.eval_metrics import *

import pandas as pd

from stable_baselines3.common.evaluation import evaluate_policy
from stable_baselines3.common.base_class import BaseAlgorithm

class StockTrader(object):
    """
    There are two way to create a StockTrader object
    (1) With untrained model : use this mode if you want to train a model
            # Mode 1: Train a policy from scratch
            trader = StockTrader(TARGET_STOCK, DQN)
            trader.learn_policy("MlpPolicy", total_timesteps=1000)
            trader.save_policy(CHECKPOINT)
            report = trader.evaluate()
            print(report)

    (2) With trained model : use this mode if you want to do inference
            # Mode 2: Load a trained model
            new_trader = StockTrader(TARGET_STOCK, DQN)
            new_trader.load_policy(CHECKPOINT, device=DEVICE)
            report = new_trader.evaluate()
            print(report)
    """

    def __init__(self,
                 stock_file        : str,
                 policy_class      : BaseAlgorithm,
                 name              : str =  "",
                 stock_with_predict: bool = False,
                 stock_state_len   : int   = 50,
                 stock_investment  : float = 10000,
                 start_data = "",
                 splite_date = "",
                 device = "auto",
                 ):

        self.name = name

        # stock & env parameters
        self.stock_file = stock_file
        self.stock_with_predict = stock_with_predict
        self.stock_state_len = stock_state_len
        self.stock_investment = stock_investment
        self.splite_date = splite_date
        self.start_data = start_data

        self.policy_class = policy_class
        self.device = device

        self.policy = None

        self.__create_env()

    def __create_env(self):

        # create env
        data = pd.read_csv(self.stock_file)

        if self.start_data != "":
            data = data[data['Date'] >= self.start_data]

        if self.splite_date != "":
            self.train_df = data[data['Date'] < self.splite_date]
            self.test_df  = data[data['Date'] >= self.splite_date]
        else:
            self.train_df = None
            self.test_df  = data

        # Train environment
        if self.train_df is not None:
            self.train_env = StockEnv(self.train_df,
                          state_length = self.stock_state_len,
                          investment = self.stock_investment,
                          with_prediction = self.stock_with_predict)
        else:
            self.train_env = None

    def used_columns(self):
        return self.train_env.used_columns()

    def get_eavl_peirod(self):
        return (self.test_df['Date'].tolist()[0], self.test_df['Date'].tolist()[-1])


    # Policy Methods
    def learn_policy(self,
            policy_tpye : str = "",
            total_timesteps : int = 10000,
            tensorboard_log = None)-> None:
        """
        Train the policy
        :return: None
        """
        print(f"Training Policy {self.name} ...")

        if self.workable():
            assert "The policy already set."

        self.train_env.reset()

        self.policy = self.policy_class(policy_tpye, 
                         self.train_env, 
                         device = self.device,
                         tensorboard_log = tensorboard_log)
        
        self.policy.learn(total_timesteps = total_timesteps)

    def load_policy(self, path) -> None:

        print(f"Loading Policy {self.name} ...")

        if self.workable():
            assert "The policy already set."
        self.policy = self.policy_class.load(path, device = self.device)

    def load_or_train_and_save(self,
                      path,
                      **kwargs):
        '''
        Load a policy ,otherwise, train a policy and save it
        :param path:
        :param **kwargs : refer method 'learn_policy'
        :return:
        '''
        if os.path.exists(path):
            self.load_policy(path)
        else:
            self.learn_policy(**kwargs)
            self.save_policy(path)

    def save_policy(self, path: str) -> None:
        self.policy.save(path)

    def workable(self) -> bool:
        '''
        This object can work only when it has a policy
        :return: bool
        '''
        return self.policy is not None

#================================================================================

from stable_baselines3 import DQN

 
PRJ_PATH = "../../"

#DATA_PATH = PRJ_PATH + "stock-price-predictor/data/"

DATA_PATH = PRJ_PATH + "stock-price-predictor/output/lstm_v1_a/"
PREDICT_PATH = PRJ_PATH + "stock-price-predictor/output/lstm_v1_a/"

TARGET_STOCK = PREDICT_PATH + "GE_2022_03_24_17_59_30_predict.csv"
CURRENT_PATH = PRJ_PATH + "stock-trading/"

CHECKPOINT   = CURRENT_PATH + "checkpoint/test.pt"
CHECKPOINT_PREDICT   = CURRENT_PATH + "checkpoint/test_with_predict.pt"

if __name__ == "__main__":

    split_date = "2017-12-30"
    start_data = "2010-01-12"
    state_len = 50
    total_timesteps = 1e5

    # Mode 1: Train a policy from scratch
    trader = StockTrader(TARGET_STOCK, DQN, stock_state_len=state_len, splite_date=split_date)
    trader.load_or_train_and_save(CHECKPOINT, policy_tpye = "MlpPolicy", total_timesteps=total_timesteps)
    evaluator = STEvaluator(TARGET_STOCK, splite_date=split_date)
    trade_hist, asset_hist, metrics = evaluator.evaluate(trader.policy, trader.used_columns(), window_size=state_len)

    print(f"Without predict: {TARGET_STOCK}, from {start_data} to {split_date}, steps: {total_timesteps}, state_len {state_len}")
    print(metrics)


    # Mode 2 Train a policy with stock price prediction
    trader_p = StockTrader(TARGET_STOCK, DQN, stock_state_len=state_len, start_data=start_data, splite_date=split_date, stock_with_predict=True)
    trader_p.load_or_train_and_save(CHECKPOINT_PREDICT, policy_tpye="MlpPolicy", total_timesteps=total_timesteps)
    evaluator = STEvaluator(TARGET_STOCK, splite_date=split_date)
    trade_hist, asset_hist, metrics = evaluator.evaluate(trader_p.policy, trader_p.used_columns(), with_prediction=True, window_size=state_len)

    print(f"With predict: {TARGET_STOCK}, from {start_data} to {split_date}, steps: {total_timesteps}, state_len {state_len}")
    print(metrics)













