import numpy as np
import pandas as pd
import math
import matplotlib
from matplotlib import pyplot as plt
import gym
from gym.spaces import Box, Discrete
from stable_baselines3.common.evaluation import evaluate_policy
from sklearn.preprocessing import MinMaxScaler
from stable_baselines3.common.env_checker import check_env

class StockEnv(gym.Env):
    
    # Constructor
    def __init__(self,
                 data,
                 state_length,
                 investment,
                 with_prediction = False ):
        '''
        INPUTS:
        - data [DataFrame]: the dataframe of the stock (from Yahool Finance).
        - state_length [int]: the size of state (trading timeframe) provided to the agent.
        - investment [int]: the investment agent is given.
        - splite_date [str]: splite date into training and test part
        '''
        # State attributes
        self.data = data
        self.scaler = MinMaxScaler(feature_range=(0,1))
        self.state_length = state_length

        self.done = False # Whether there is more state
        self.t = self.state_length # Current date where the agent is at
        self.ACTION_SPACE_SIZE = 2 # 2 actions avaliable
        self.price_prediction_cols = ['Predict1', 'Predict2', 'Predict3', 'Predict4', 'Predict5']

        self.with_prediction = with_prediction

        self.__format_state()
        
        # Agent attributes
        self.wallet_init = investment # For resetting
        self.wallet = investment # For making updates
        self.portfolio = 0 # Stock owned by agent
        self.history = {'Buy': [[],[],[]], 'Sell': [[],[],[]]} # Saves the trading history. The list[0] stores the date, list[1] stores the price
        self.action_space = Discrete(2)


        if with_prediction:
            self.observation_space = Box(low=-np.inf, high=np.inf, shape=(state_length+5, len(self.used_columns())), dtype=np.float32)
        else:
            self.observation_space = Box(low=-np.inf, high=np.inf, shape=(state_length, len(self.used_columns())), dtype=np.float32)

        check_env(self, warn=True)


    def used_columns(self):

        if self.with_prediction:
            return ["Open"]
        else:
            return ["Open"]

    
    def __format_state(self):
        #state = self.data[self.t - self.state_length : self.t].Close
        state = self.data[self.t - self.state_length: self.t][self.used_columns()]


        if self.with_prediction:
            prediction = self.data[self.price_prediction_cols].iloc[self.t].values
            state = np.array(state.iloc[:,0].tolist() + [1,1,1,1,1])
            self.state = state.reshape((len(state), len(self.used_columns())))
        else:
            self.state = np.array(state).reshape((self.state_length, len(self.used_columns())))

    def step(self, action, deploy=False):
        '''
        INPUTS:
        - action [int]: the action took by the agent. 
            - action = 0: buy
            - action = 1: sell
        OUTPUTS:
        - state: the new state of the agent (one day foward)
        - reward: the reward of the action
        - done: whether the game is over
        - info: provides info of the state ('wallet' ,'portfolio','current day')
        '''
        
        # Check if the agent can buy any stock
        # Returns: Boolean. True if can buy, False if not
        def can_buy(self):
            if self.wallet >= self.data.iloc[self.t].Open:
                    return True
            else:
                return False
    
        # Check if the agent can sell any stock
        # Returns: Boolean. True if can sell, False if not
        def can_sell(self):
            if self.portfolio > 0:
                return True
            else:
                return False
        
        # Increment state by 1 step (foward 1 day)
        def step_state(self):
            self.t += 1
            if self.t == self.data.shape[0]-1: # No more data
                self.done = True
            self.__format_state()
            
        # Computes the total balance: wallet + holdings
        def total_balance(self, price):
            '''
            INPUTS:
            - price [int]: price of 1 stock
            '''
            #current_price = self.data.iloc[0].Open # Price of 1 stock
            return self.wallet + price*self.portfolio

        def buy_size(action):
            return self.wallet // price_t1

        def sell_size(action):
            return self.portfolio
        
        reward = 0 # initialize reward
        price_t1 = self.data.iloc[self.t].Open # Today's price
        date = self.data.iloc[self.t].Date
        step_state(self) # Proceed to next state
        price_t2 = self.data.iloc[self.t].Open # Tomorrow's price
        
        if action == 0: # buy stock and sell it at the second day

            size = buy_size(action)

            if self.portfolio == 0:
                self.wallet -= price_t1 * size
                self.portfolio += size

                self.history['Buy'][0].append(date)
                self.history['Buy'][1].append(price_t1)
                self.history['Buy'][2].append(size)
            else:
                pass
                # do nothing, just hold

            # reward is the regret
            reward = 100 * (price_t2 - price_t1) / price_t1

        elif action == 1 and can_sell(self): # sell stock

            size = sell_size(action)

            if self.portfolio != 0:
                self.wallet += price_t1 * size
                self.portfolio -= size

                self.history['Sell'][0].append(date)
                self.history['Sell'][1].append(price_t1)
                self.history['Sell'][2].append(size)
            else:
                # do nothing, just hold
                pass

            # reward is the opportunity cost
            reward = 100 * (price_t1 - price_t2) / price_t1

        else: # hold stock
            reward = 0
            
        net_worth = total_balance(self, price_t1)
        if net_worth < 0.90*self.wallet_init or self.t >= len(self.data)-2:
            self.done = True
        else:
            self.done = False
        
        info = {'net worth': net_worth,
                'wallet': self.wallet, 
                'portfolio': self.portfolio,
                'current day': self.t,
                'reward': reward}
        
        return self.state, reward, self.done, info


    # Reset the environment.
    def reset(self, random_day=False):

        # Reset state attributes
        if random_day == False:
            self.t = self.state_length
        else: # reset on a random day
            day = np.random.randint(self.state_length, len(self.data))
            self.t = day

        self.__format_state()

        self.done = False
            
        # Reset agent attributes
        self.wallet = self.wallet_init
        self.portfolio = 0
        return self.state
    
    def render(self, mode='console'):

        price = self.data.iloc[self.t].Open
        networth = self.wallet + price*self.portfolio

        for i in range(len(self.history['Buy'])):
            buy = self.history['Buy'][i]

            if i < len(self.history['Sell']):
                sell = self.history['Sell'][i]
                print(f"Buy  at {buy[0]}  price : {buy[1]}  size : {buy[2]} | "
                      f"Sell at {sell[0]} price : {sell[1]} size : {sell[2]} ==> Profit : {buy[2]*(sell[1]-buy[1])}")
            else:
                print(f"Buy  at {buy[0]}  price : {buy[1]}  size : {buy[2]}")

        print('Day: {}, Networth: {}, Wallet: {}, Portfolio: {}'.format(self.t, networth, self.wallet, self.portfolio))