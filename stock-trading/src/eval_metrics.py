import pandas as pd
import numpy as np

from datetime import datetime

from stable_baselines3.common.base_class import BaseAlgorithm


class STEvaluator(object):

    def __init__(self, stock_file, splite_date):
        data = pd.read_csv(stock_file)
        self.data = data[data['Date'] >= splite_date].reset_index()

    def evaluate(self, algo, cols = ['Open'], window_size = 30, with_prediction = False, init_money = 100000):
        '''
        :param algo, support BaseAlgorithm::predict() method :
        :param window_size:
        :param init_money:
        :return: trade_hist, asset_hist, metrics
        '''
        print(f"Evaluating Policy ...")

        stock_size = 0
        money = init_money

        trade_hist = [[], []]
        asset_hist = []

        for i in range(self.data.shape[0] - window_size - 1):

            window = self.data[i:i + window_size]

            if with_prediction:

                his_data = window[cols].iloc[:,0].tolist() \
                           + window.iloc[-1][-5:].tolist()

                his_data = np.array(his_data).reshape(len(his_data), 1)
                action = algo.predict(his_data, deterministic=True)

            else:
                action = algo.predict(window[cols].values.reshape(window_size, len(cols)), deterministic=True)

            next_day = self.data.iloc[i + window_size + 1] #window.iloc[-1]
            next_day_price = next_day['Open']

            if action[0] == 0: # buy
                if stock_size == 0:
                    size = money // next_day_price
                    stock_size = size
                    money -= stock_size * next_day_price
            else: #sell
                 if stock_size != 0:
                     money += stock_size * next_day_price
                     stock_size = 0

            trade_hist.append((next_day['Date'], action))
            asset_hist.append(money + stock_size * next_day_price)

        #===================
        year_duration = self.duration_as_year()

        last_asset = asset_hist[-1]
        profit = last_asset - init_money

        year_return_rate = (100 * ( profit / init_money)) / year_duration
        SharpRatio = STEvaluator.CalcSharpRatio(asset_hist, init_money)
        SharpRatio1 = STEvaluator.CalcSharpRatio(asset_hist, init_money, risk_free_rate=1 * year_duration)
        SharpRatio2 = STEvaluator.CalcSharpRatio(asset_hist, init_money, risk_free_rate=2 * year_duration)
        SharpRatio3 = STEvaluator.CalcSharpRatio(asset_hist, init_money, risk_free_rate=3 * year_duration)

        metrics ={
                    'ARR': round(year_return_rate, 2),
                    'SR':  round(SharpRatio, 2),
                    'SR1': round(SharpRatio1, 2),
                    'SR2': round(SharpRatio2, 2),
                    'SR3': round(SharpRatio3, 2),
                    'MDD': round(STEvaluator.CalcMDD(asset_hist), 2),
                    'MRR' : round(STEvaluator.CalcMaxReturn(asset_hist, init_money), 2),
                    'MLR'   : round(STEvaluator.CalcMaxLoss(asset_hist, init_money), 2),
                    'Start' : str(self.start_date().date()),
                    'End'   : str(self.end_date().date()),
                    'Dura(Y)' : year_duration,
                    'Trade': len(trade_hist) // 2,
                    "Init": init_money,
                    "Last": int(last_asset),
               }

        return trade_hist, asset_hist, metrics


    def duration_as_year(self):
        duration = self.end_date() - self.start_date()
        return round(duration.days / 356, 2)

    def start_date(self):
        return self.str2date(self.data['Date'].values[0])

    def end_date(self):
        return self.str2date(self.data['Date'].values[-2])

    def str2date(self, date_str):
        return datetime.strptime(date_str, "%Y-%m-%d")

    @staticmethod
    def CalcMDD(networth):
        '''
        Calculate Maxium Draw
        :param networth:
        :return:
        '''
        df = pd.Series(networth, name="nw").to_frame()

        max_peaks_idx = df.nw.expanding(min_periods=1).apply(lambda x: x.argmax()).fillna(0).astype(int)
        df['max_peaks_idx'] = pd.Series(max_peaks_idx).to_frame()

        nw_peaks = pd.Series(df.nw.iloc[max_peaks_idx.values].values, index=df.nw.index)

        df['dd'] = ((df.nw-nw_peaks)/nw_peaks)
        df['mdd'] = df.groupby('max_peaks_idx').dd.apply(lambda x: x.expanding(min_periods=1).apply(lambda y: y.min())).fillna(0)

        return 100 * df['mdd'].min()

    @staticmethod
    def CalcSharpRatio(networth, init_asset, risk_free_rate = 0.0):
        '''
        Calculate Sharpe Ratio
            Less than 1       : Bad
                      1 – 1.99: Adequate/good
                      2 – 2.99: Very good
            Greater than 3    : Excellent
        : param networth:
        : risk_free_rate: risk free rate across year
        :return sharpe ratio
        '''

        reward = (100 * (np.array(networth) - init_asset)) / init_asset

        std = reward.std()
        mean = reward.mean() - risk_free_rate

        if std == 0:
            return 0
        else:
            return mean / std

    @staticmethod
    def CalcMaxReturn(networth, init_asset):
        '''
        Max return by percentage
        :param networth: asset value
        :param init_asset: initial asset value
        :return:
        '''
        reward = np.array(networth) - init_asset
        return 100 * max(reward/init_asset)

    @staticmethod
    def CalcMaxLoss(networth, init_asset):
        '''
        Max loss by percentage
        :param networth: asset value
        :param init_asset: initial asset value
        :return:
        '''
        reward = np.array(networth) - init_asset
        return 100 * min(reward/init_asset)





















