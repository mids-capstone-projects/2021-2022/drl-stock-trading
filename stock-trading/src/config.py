from stable_baselines3 import DQN
from src.baseline import BS_NaiveHold, BS_AverageDayLine


###########
# Only need config this path

PRJ_PATH = "/content/gdrive/MyDrive/Capstone/Workspace/drl-stock-trading/"


########################
STOCK_PRICE_PREDICTION = PRJ_PATH + "stock-price-predictor/"
STOCK_TRADING = PRJ_PATH + "stock-trading/"
ST_REPORT = STOCK_TRADING + 'report/'
CP     = STOCK_TRADING + "checkpoint/"
DATA    = STOCK_PRICE_PREDICTION + "output/lstm_v1_a/"

DRL_MODELS = {

    "default" : {
                     "name" : "sample",
                     "CP":CP,
                     "DATA" : DATA,
                     "start_date"  : "2010-01-12",
                     "splite_date" : "2017-12-30"
                 },


    #"DQN_NFLX_30_100k"  : {"algo":DQN,  "stock":"NFLX", "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    #"DQN_NFLX_50_100k"  : {"algo":DQN,  "stock":"NFLX", "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    "DQN_NFLX_PRE_A_30_100k" : {"algo":DQN,  "stock":"NFLX", "predict": True, "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    "DQN_NFLX_PRE_A_50_100k" : {"algo":DQN,  "stock":"NFLX", "predict": True, "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    #"DQN_NFLX_PRE_B_30_100k" : {"algo":DQN,  "stock":"NFLX", "predict": True, "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    #"DQN_NFLX_PRE_B_50_100k" : {"algo":DQN,  "stock":"NFLX", "predict": True, "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    #"BASELINE_NFLX"  : {"algo":BS_NaiveHold, "stock":"NFLX", "state_len" : 50},
	#"BASELINE_NFLX_D30"  : {"algo":BS_AverageDayLine, "stock":"NFLX", "state_len" : 30},
    #"BASELINE_NFLX_D50"  : {"algo":BS_AverageDayLine, "stock":"NFLX", "state_len" : 50},

    #"DQN_GE_30_100k"  : {"algo":DQN,  "stock":"GE", "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    #"DQN_GE_50_100k"  : {"algo":DQN,  "stock":"GE", "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    "DQN_GE_PRE_A_30_100k" : {"algo":DQN,  "stock":"GE", "predict": True, "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    "DQN_GE_PRE_A_50_100k" : {"algo":DQN,  "stock":"GE", "predict": True, "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    #"DQN_GE_PRE_B_30_100k" : {"algo":DQN,  "stock":"GE", "predict": True, "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    #"DQN_GE_PRE_B_50_100k" : {"algo":DQN,  "stock":"GE", "predict": True, "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    #"BASELINE_GE"    : {"algo":BS_NaiveHold, "stock":"GE", "state_len" : 50},
	#"BASELINE_GE_D30"  : {"algo":BS_AverageDayLine, "stock":"GE", "state_len" : 30},
    #"BASELINE_GE_D50"  : {"algo":BS_AverageDayLine, "stock":"GE", "state_len" : 50},

    #"DQN_GOOG_30_100k"  : {"algo":DQN,  "stock":"GOOG", "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    #"DQN_GOOG_50_100k"  : {"algo":DQN,  "stock":"GOOG", "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    "DQN_GOOG_PRE_A_30_100k" : {"algo":DQN,  "stock":"GOOG", "predict": True, "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    "DQN_GOOG_PRE_A_50_100k" : {"algo":DQN,  "stock":"GOOG", "predict": True, "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    #"DQN_GOOG_PRE_B_30_100k" : {"algo":DQN,  "stock":"GOOG", "predict": True, "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    #"DQN_GOOG_PRE_B_50_100k" : {"algo":DQN,  "stock":"GOOG", "predict": True, "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    #"BASELINE_GOOG"    : {"algo":BS_NaiveHold, "stock":"GOOG", "state_len" : 50},
	#"BASELINE_GOOG_D30"  : {"algo":BS_AverageDayLine, "stock":"GOOG", "state_len" : 30},
    #"BASELINE_GOOG_D50"  : {"algo":BS_AverageDayLine, "stock":"GOOG", "state_len" : 50},

    #"DQN_JPM_30_100k"  : {"algo":DQN,  "stock":"JPM", "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    #"DQN_JPM_50_100k"  : {"algo":DQN,  "stock":"JPM", "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    "DQN_JPM_PRE_A_30_100k" : {"algo":DQN,  "stock":"JPM", "predict": True, "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    "DQN_JPM_PRE_A_50_100k" : {"algo":DQN,  "stock":"JPM", "predict": True, "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    #"DQN_JPM_PRE_B_30_100k" : {"algo":DQN,  "stock":"JPM", "predict": True, "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    #"DQN_JPM_PRE_B_50_100k" : {"algo":DQN,  "stock":"JPM", "predict": True, "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    #"BASELINE_JPM"    : {"algo":BS_NaiveHold, "stock":"JPM", "state_len" : 50},
	#"BASELINE_JPM_D30"  : {"algo":BS_AverageDayLine, "stock":"JPM", "state_len" : 30},
    #"BASELINE_JPM_D50"  : {"algo":BS_AverageDayLine, "stock":"JPM", "state_len" : 50},
 
    #"DQN_PFE_30_100k"  : {"algo":DQN,  "stock":"PFE", "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    #"DQN_PFE_50_100k"  : {"algo":DQN,  "stock":"PFE", "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    "DQN_PFE_PRE_A_30_100k" : {"algo":DQN,  "stock":"PFE", "predict": True, "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    "DQN_PFE_PRE_A_50_100k" : {"algo":DQN,  "stock":"PFE", "predict": True, "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    #"DQN_PFE_PRE_B_30_100k" : {"algo":DQN,  "stock":"PFE", "predict": True, "state_len" : 30, "type":"MlpPolicy",  "step":1e5},
    #"DQN_PFE_PRE_B_50_100k" : {"algo":DQN,  "stock":"PFE", "predict": True, "state_len" : 50, "type":"MlpPolicy",  "step":1e5},
    #"BASELINE_PFE"    : {"algo":BS_NaiveHold, "stock":"PFE", "state_len" : 50},
	#"BASELINE_PFE_D30"  : {"algo":BS_AverageDayLine, "stock":"PFE", "state_len" : 30},
    #"BASELINE_PFE_D50"  : {"algo":BS_AverageDayLine, "stock":"PFE", "state_len" : 50},
 
}