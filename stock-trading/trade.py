


import numpy as np
import pandas as pd
import math
import matplotlib
import torch as th
from matplotlib import pyplot as plt

from sklearn.preprocessing import MinMaxScaler

from stable_baselines3 import DQN
from stable_baselines3.common.env_checker import check_env

from src.stockenv import StockEnv
from src.stock_trading import StockTrader


####
# configuration

PRJ_PATH = "../"

DATA_PATH = PRJ_PATH + "stock-price-predictor/data/"
PREDICT_PATH = PRJ_PATH + "stock-price-predictor/output/lstm_v1_a/"

TARGET_STOCK = DATA_PATH + "NFLX.csv"
CURRENT_PATH = PRJ_PATH + "stock-trading/"

CHECKPOINT = CURRENT_PATH + "./checkpoint/dqn_NFLX.pt"

DEVICE = th.device('cuda' if th.cuda.is_available() else 'cpu')
print(f"Device: {DEVICE}")



if __name__ == "__main__":

    # create env
    data = pd.read_csv(TARGET_STOCK)
    env = StockEnv(data, state_length=50, investment=1000)
    check_env(env, warn=True)

    # create policy
    model = DQN("MlpPolicy", env, verbose=1)

    # gen trader
    trader = StockTrader(model)
    trader.learn(total_timesteps = 1000)

    # Save
    CHECKPOINT = f"./checkpoint/dqn_NFLX.pt"
    trader.save(CHECKPOINT)

    new_trader = StockTrader()
    new_trader.load(CHECKPOINT, DQN, device=DEVICE)

    report = new_trader.evalute(env)
    print(report)





























