import getopt
import sys

from config import MODELS, STOCK_LIST_ALL
from importlib import import_module

def run_model(model, stock):

    # Create Model
    class_str = "models.{}".format(MODELS[model]["model"])
    module_path, class_name = class_str.rsplit('.', 1)
    module = import_module(class_str)
    instance = getattr(module, class_name)(model)

    # train
    if stock == "all":
        stocks = STOCK_LIST_ALL
    else:
        assert stock in STOCK_LIST_ALL
        stocks = [stock]

    for s in stocks:
        instance.train(s)



if __name__ == "__main__":

    run_model("lstm_v1_a", "all")


