

# Path Config

WORKSPACEK = '/content/drive/MyDrive/Capstone/Workspace/stock-market-reinforcement-learning/stock-price-predictor'

SRC = '{}/src'.format("WORKSPACEK")
DATA ='{}/data'.format("WORKSPACEK")
OUTPUT ='{}/output'.format("WORKSPACEK")
model ='{}/model'.format("WORKSPACEK")


# Stocks Config

STOCK_LIST_ALL = [
    "GE",
    "JPM",
    "GOOG",
    "NFLX",
    "PFE",
]

# Models Config

MODELS = {

    "lstm_v1_a" : {"model" : "lstm_v1", "epoch"  : 200, "break" : "2018-01-01"},
    "lstm_v1_b" : {"model" : "lstm_v1", "epoch"  : 600, "break" : "2018-01-01"},
    "lstm_v2_a" : {"model" : "lstm_v2", "epoch"  : 200, "break" : "2018-01-01"},
    "lstm_v2_b" : {"model" : "lstm_v2", "epoch"  : 600, "break" : "2018-01-01"}
  
}
























