"""
Author          : Martin Guo
Date created    : 2021-11-06
Date last modified: 2021-11-06
"""
import os
import math
import numpy as np
import pandas as pd

from sklearn.metrics import mean_squared_error, mean_absolute_error

from datetime import datetime
from abc import ABC, abstractmethod

from config import MODELS

class StockPredictModel(ABC):

    def __init__(self, model_name):
        self.model_name = model_name
        self.config = MODELS[model_name]


    @abstractmethod
    def fit(self, model, Train_X, Train_Y, Test_X, Test_Y):
        pass

    @abstractmethod
    def get_model(self):
        pass



    #===================================================


    def train(self, stock_name):

        stock = self.read_data(stock_name)

        Train_X, Train_Y, Test_X, Test_Y = self.get_dataset(stock, self.config["break"])

        model = self.get_model()

        hist = self.fit(model, Train_X, Train_Y, Test_X, Test_Y)

        self.predict(hist, model, Test_X, Test_Y)

        self.write_output(stock, stock_name)


    def read_data(self, stock_name):

        INPUT_FILE = "../data/{}.csv".format(stock_name)
        data = pd.read_csv(INPUT_FILE)
        data["Date"] = pd.to_datetime(data["Date"])

        return data


    def get_output_folder(self):
        path = "../output/{}".format(self.model_name)
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    def write_output(self, stock, stock_name):

        output_path = self.get_output_folder()
        timestamp = self.get_timestamp()

        for i in range(1, 6):
            stock['Predict{}'.format(i)] = 0
            stock['Predict{}'.format(i)][-1 * len(self.prediction):] = [x[i - 1] for x in self.prediction]

        file_name = "{}/{}_{}_predict.csv".format(output_path, stock_name, timestamp)

        print("Write date into file {}".format(file_name))
        stock.to_csv(file_name, index=False)

        with open("{}/{}_{}_README.txt".format(output_path, stock_name, timestamp), "w") as f:
            f.write("MODEL_NAME:    {}\n".format(self.model_name))
            f.write("Break_Date:    {}\n".format(self.config["break"]))
            f.write("EPOCH:         {}\n".format(self.config["epoch"]))
            f.write("STOCK_NAME:    {}\n".format(stock_name))

            f.write(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n")
            f.write('RMSE: {}\n'.format(self.rmse))
            f.write('MAPE: {}\n'.format(self.mape))


    def predict(self, hist, model, Test_X, Test_Y):
        self.prediction = model.predict(Test_X)
        self.rmse = math.sqrt(mean_squared_error(Test_Y.reshape(-1, 5), self.prediction))
        self.mape = np.mean(np.abs(self.prediction - Test_Y.reshape(-1, 5)) / np.abs(Test_Y.reshape(-1, 5)))

        print('RMSE: {}'.format(self.rmse))
        print('MAPE: {}'.format(self.mape))

    def get_timestamp(self):
        return datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

    def get_dataset(self, Data, Date):

        Train_Data = Data['Adj. Close'][Data['Date'] < Date].to_numpy()
        Data_Train = []
        Data_Train_X = []
        Data_Train_Y = []
        for i in range(4, len(Train_Data) - 2):
            try:
                Data_Train_X.append(Train_Data[i - 4: i + 1])
                Data_Train_Y.append(Train_Data[i - 3: i + 2])
            except:
                pass

        # if len(Data_Train[-1]) < 5:
        #    Data_Train.pop(-1)

        # Data_Train_X = Data_Train[0: -1]
        Data_Train_X = np.array(Data_Train_X)
        Data_Train_X = Data_Train_X.reshape((-1, 5, 1))
        # Data_Train_Y = Train_Data[1: len(Train_Data)]
        Data_Train_Y = np.array(Data_Train_Y)
        Data_Train_Y = Data_Train_Y.reshape((-1, 5, 1))

        Test_Data = Data['Adj. Close'][Data['Date'] >= Date].to_numpy()
        Data_Test = []
        Data_Test_X = []
        Data_Test_Y = []
        for i in range(4, len(Test_Data) - 2):
            try:
                # Data_Test.append(Test_Data[i-4: i+1])
                Data_Test_X.append(Test_Data[i - 4: i + 1])
                Data_Test_Y.append(Test_Data[i - 3: i + 2])
            except:
                pass

        # if len(Data_Test[-1]) < 5:
        #    Data_Test.pop(-1)

        # Data_Test_X = Data_Test[0: -1]
        Data_Test_X = np.array(Data_Test_X)
        Data_Test_X = Data_Test_X.reshape((-1, 5, 1))
        # Data_Test_Y = Data_Test[1: len(Data_Test)]
        Data_Test_Y = np.array(Data_Test_Y)
        Data_Test_Y = Data_Test_Y.reshape((-1, 5, 1))

        return Data_Train_X, Data_Train_Y, Data_Test_X, Data_Test_Y






















