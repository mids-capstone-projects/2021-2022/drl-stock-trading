
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime

import tensorflow as tf
from sklearn.metrics import mean_squared_error, mean_absolute_error

import warnings


warnings.filterwarnings('ignore')


from models.model import  StockPredictModel

class lstm_v1(StockPredictModel):

    def __init__(self, model_name):
        # Call super init
        super(lstm_v1, self).__init__(model_name)



    def fit(self, model, Train_X, Train_Y, Test_X, Test_Y):

        def scheduler(epoch):

            if epoch <= 150:
                lrate = (10 ** -5) * (epoch / 150)
            elif epoch <= 400:
                initial_lrate = (10 ** -5)
                k = 0.01
                lrate = initial_lrate * math.exp(-k * (epoch - 150))
            else:
                lrate = (10 ** -6)

            return lrate

        callback = tf.keras.callbacks.LearningRateScheduler(scheduler)

        hist = model.fit(Train_X,
                         Train_Y,
                         epochs=self.config["epoch"],
                         validation_data=(Test_X, Test_Y),
                         callbacks=[callback])

        return hist


    def get_model(self):

        model = tf.keras.models.Sequential([
            tf.keras.layers.LSTM(200, input_shape=(5, 1), activation=tf.nn.leaky_relu, return_sequences=True),
            tf.keras.layers.LSTM(200, activation=tf.nn.leaky_relu),
            tf.keras.layers.Dense(200, activation=tf.nn.leaky_relu),
            tf.keras.layers.Dense(100, activation=tf.nn.leaky_relu),
            tf.keras.layers.Dense(50, activation=tf.nn.leaky_relu),
            tf.keras.layers.Dense(5, activation=tf.nn.leaky_relu)
        ])

        model.compile(optimizer=tf.keras.optimizers.Adam(),
                      loss='mse',
                      metrics= [tf.keras.metrics.RootMeanSquaredError()])

        return model








































