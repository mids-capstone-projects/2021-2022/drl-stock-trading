# Reinforcement Learning in Stock Trading 



In this project, we built a modular reinforcement learning system with the support of additional integrated predictive models to tackle stock trading. More specifically, an LSTM model was trained to predict the future stock market and integrated into offline DQN (Deep Q-learning) reinforcement learning
agents. Commonly used financial metrics such as annual rate of return, sharp ratio, and maximum drawdown rate were calculated to evaluate the performance of models and agents.

<img src="./img/pipeline.png" alt="Trading Pipeline" style="zoom:75%;" />

## Table of Contents
1. [Motivation](#motivation)
2. [Stock Price Predictor](#stock-price-predictor)
    - [Add New Stock Data](#add-new-stock-data)
    - [Add Your Machine Learning Algorithms](#add-your-machine-learning-algorithms)
    - [Config Training and Predicting](#config-training-and-predicting)
    - [Output and How to Evaluate](#output-and-how-to-evaluate)
    - [Run It](#run-it)
3. [Stock Trading](#stock-trading)
    - [Stock Trader Object](#stock-trader-object)
    - [How to Evaluate](#how-to-evaluate)
    - [Using on large scale with config file](#using-on-large-scale-with-config-file)

## Motivation
Applying artificial intelligence to stock trading is always an exciting topic. However, it requires extra effort to pre-process the data and evaluate the result. Reinforcement learning exacerbates the problem since it requires a trading environment for training which supervised learning models do not need. We hope to provide a concise and ready-to-use framework for anyone who wants to play reinforcement learning on stock trading with this project. 

This stock trading framework includes an OpenAI Gym compatible stock trading environment, a trading strategy template, a set of evaluation metrics, and a mechanism to run and evaluate the strategy on the scale.

## Stock Price Predictor

This stock price predictor provide a PyTorch framework to predict stock price with various machine learning modules.  User can add more data and their own algorithms. 

### Add New Stock Data

All raw data should put in  `./data` folder.  The historical  price for each stock is kept in a separate file.  After put the data file, you can add them to training and evaluation process.  **Config Training and Predicting**  will introduce how to add them to the process. 

#### How to name stock file

You can name the stock file however you like, but we recommend following certain rules, such as using the stock symbol on Yahoo. This increases the readability and maintainability of the code.

#### Stock file format 

The stock file is in csv format, each column are separated by comma . The following columns are required (case sensitive):

	- "Date"	: Date of current record.
	- "Open"	: Open price
	- "High"	: The price of first deal of that day
	- "Low"		: Lowest price of that day
	- "Close"	: The price of last deal of that day
	- "Adj. Close"	: Price amends a stock's closing price to reflect that stock's value after accounting for any corporate actions.
	- "Volume" : "Volume measures the number of shares traded in a stock or contracts traded in futures or options."


#### How to get the stock data

[Yahoo Finance](https://finance.yahoo.com/) is a reliable source for download stock data. You can find target stock by searching with its name.  Once you land on the main page of a stock, you can find "Historical Prices" tab . The download link is just above the stock price list. 



###  Add Your Machine Learning Algorithms

You can easily add your own machine learning algorithms to the framework with minimum code. The framework will take care of training, evolution, prediction and report generation.  

####  Add your code

All model should put under `./src/models` .  Each python scrape file should only contain **one** model.  

**The name of the file should identical with model class name. **  For example,  class `lstm_v1` is put in the `lstm_v1.py` file. The framework need create object of your model according to this assumption. 

####  How to write a new  model

All stock predicting model should inherent from `StockPredictModel` which is defined  in `./src/models/model.py`.  Your class only need implement two functions: 

- `get_mode` : Generate a PyTorch model and return it. 
- `fit` : Train the model return by *get_model*.



###  Config Training and Predicting 

You can run different stocks, algorithms and parameters in one process with proper config file. The config file is `./src/config.py`.   There are three sections in the config file:

- Path : Local path
- Stocks : stocks to be used
- Models : different algorithms 

There are two things you should take care:

1.  Each item in `MODELS` presents a algorithm. The same algorithm with the different hyper-parameter are considered as different models. 
2.  Each model will be trained on only one model.  This means a model would be trained and evaluate 5 times on 5 different stocks. 

#### Stocks 

You can simple put the stock's name in `STOCK_LIST_ALL`. The name will be associated with data file in  `./data` folder.  

#### Models 

The model is configured as python dictionary.  The key is the name of the model,  the value is detailed configuration for the model. 

- model :  The name of the model, it should be matched with model's python script in `./src/models`. 

- epoch :  Training epoch 

- break :  How to split training and testing data. The data before the *break* is training data, the data after the `break` is testing data. 

  


###  Output and How to Evaluate

The output will be placed under `./output`. Each model would have its onw subfolder.  The subfolder's name is exactly identical with its name in configured in config.py.  Each stock would generate 2 files:

 - predict.csv:  The stock price prediction.  Original data file  with five columns (Predict1 ~ Predict5)  appended. The new columns are predicted stock prices in next 5 days. 
 - README.txt:  Configuration of this training and performance. 

#### Fila name format

The output files name follows naming rules: 

`<Stock Name>_<Timestamp (YYYY_MM_DD_HH_mm_ss)>_<[predict|README]>.<[csv|txt]>`

#### Metrics

In the README file, there are two metrics to evaluate the performance of the model 

- [RMSE (Root Mean Square Error)](https://en.wikipedia.org/wiki/Root-mean-square_deviation): The root-mean-square deviation or root-mean-square error is a frequently used measure of the differences between values predicted by a model or an estimator and the values observed

- [MAPE (Absolute Percentage Error)](https://en.wikipedia.org/wiki/Mean_absolute_percentage_error) : The mean absolute percentage error, also known as mean absolute percentage deviation, is a measure of prediction accuracy of a forecasting method in statistics.



### Run It

Call `run_model` function in `./src/main.py`.   There are two parameters for this function: 

- model:  run which model 
- stock: run which stock.  "All" means to run all stocks configured in `STOCK_LIST_ALL` in `config.py`.  

## Stock Trading

### Stock Trader Object 

The core features of stock trading were implemented in `StockTrader` class. `StockTrader` will create a trading  environment from `OCHL` format stock data (csv) automatically.  A typical code to create a StockTrader  object is : 

```python
trader = StockTrader(TARGET_STOCK,
                     DQN,
                     stock_state_len=30,
                     splite_date=split_date)
```

- `TARGET_STOCK`: A string of `OCHL` format stock data (csv)  file.

- `DQN`: A Stable Baseline3 compatible reinforcement policy. 

- `stock_state_len`: Trading stocking according to last *n* days' history. 

- `splite_date`: A breaking date.  The data before this day, inclusive, will be used as training data. 

  ​                         The left data will be used for evaluation. 



After creation a trader object, user can use `learn_policy` to train the policy. Or, user can use `load_or_train_and_save` to get workable StockTrader. The function will load a trained policy if there is a usable checkpoint at appointed  path, otherwise, it will train a policy from scratch and save it to the appointed path. 

Sample code to load or train a StockTrader: 

```python
trader.load_or_train_and_save(CHECKPOINT, 
                              policy_tpye = "MlpPolicy", 
                              total_timesteps=1000)
```

### How to Evaluate

The class `STEvaluator` is used to evaluate trading policies. It would generate a set of matrices according to the policies' performances.  


#### Create a Evaluator

The code to create a `STEvaluator` : 

```python
evaluator = STEvaluator(TARGET_STOCK, start_date=split_date)
```

- `TARGET_STOCK`: A string of `OCHL` format stock data (csv)  file.

- `splite_date`: A breaking date. Data after this date will be used to evaluate policies. 


The meaning of `Splite_date` is different between `StockTrader` and `STEvaluator`. Hence the user can use the same data file and parameters to create objects from these classes.

#### Evaluate

Once create an evaluator, user can use it to evaluate a policy 

```
trade_hist, asset_hist, metrics 
	= evaluator.evaluate(trader.policy, window_size=30)
```

- `trader.policy`: Trained policy wrapped by `StockTrader`
- `window_siz`e: Expose how many days of history data to the policy.  It should larger than parameter `stock_state_len`, when creating  the `StockTrader` object. 

#### Results

- `trade_hist`: Buying and Selling history. 
- `asset_hist`: Changing history of assets. 
- metrics: various matrices:
  - ARR: Annual Return Rate
  - SR: SharpRatio (Annual Risk Free Rate is 0)
  - SR1: SharpRatio (Annual Risk Free Rate is 1%)
  - SR2: SharpRatio (Annual Risk Free Rate is 2%)
  - SR3: SharpRatio (Annual Risk Free Rate is 3%)
  - MDD: Maximum  Drawdown
  - MRR: Maximum Return Rate
  - MLR: Maximum Loss Rate

#### Baseline 

No matter how fancy the trading policy are, they need to beat some baselines to be useful. `BaselineStratage` works as a baseline class template. With it, user only needs to implement `predict` function.  A naïve buy-and-hold policy can be implemented as:

```python
class BS_NaiveHold(BaselineStratage):

    def predict(self, window : pd.DataFrame, **kwargs):
        return (0, None)
```

User can also use `STEvaluator` to evaluate these baselines strategies with the same data and almost the same code. 

```python
evaluator = STEvaluator(TARGET_STOCK, splite_date=split_date)
trade_hist, asset_hist, metrics = evaluator.evaluate(BS_NaiveHold(), window_size=30)
```

### Using on large scale with config file

Sometimes, we need to evaluate more than one trading polices on many stocks. The framework also enables users to scale their experiments with configuring file mode. 

```python
PRJ_PATH = "../../"
STOCK_TRADING = "../"
ST_REPORT = STOCK_TRADING + 'report/'
CP = STOCK_TRADING + "checkpoint/"
DATA = PRJ_PATH    + "stock-price-predictor/data/"

DRL_MODELS = {

    "default" : {
                     "name" : "sample",
                     "DATA": DATA,
                     "CP":CP,
                     "total_timesteps":1000
                 },


    "DQN_NFLX" : {"algo":DQN,  "stock":"NFLX", "data": DATA+"NFLX.csv", "type":"MlpPolicy"}

}
```


- `default`: Configuration some path to load data and load/save models. 
- `DQN_NFLX`: Configuration of a policy.  This framework would train different models for different stocks. Hence,  the same reinforcement learning algorithm would generate different models.  



## Authors

[Martin Guo](https://github.com/gamecicn), [Leon Zhang](https://github.com/leonz12345), [Xinyi Pan](https://github.com/solaris-2578), [Wilson Huang](https://github.com/yichenghuang980)











